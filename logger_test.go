package go_logger

import "testing"

func TestCanLog(t *testing.T) {
	logLevels := []LogLevel{OFF, TRACE, DEBUG, INFO, WARN, ERROR, CRITICAL}

	for i, currentLogLevel := range logLevels {
		logger := Logger{LogLevel: currentLogLevel}
		for j, testLevel := range logLevels {
			if i > j && logger.canLog(testLevel) {
				t.Errorf("logger should not log for %s when set to %s", testLevel, currentLogLevel)
			}
		}
	}
}

func TestMapLogLevel(t *testing.T) {
	traces := []string{"trace", "TRACE"}
	debugs := []string{"debug", "DEBUG"}
	warns := []string{"warn", "WARN"}
	infos := []string{"info", "INFO"}
	errors := []string{"error", "ERROR"}
	criticals := []string{"critical", "CRITICAL"}
	offs := []string{"off", "OFF"}
	unknowns := []string{"foo"}
	var unknown LogLevel = -1
	m := map[LogLevel][]string{
		TRACE:    traces,
		DEBUG:    debugs,
		WARN:     warns,
		INFO:     infos,
		ERROR:    errors,
		CRITICAL: criticals,
		OFF:      offs,
		unknown:  unknowns,
	}

	for logLevel, values := range m {
		for _, value := range values {
			t.Run(value, func(t *testing.T) {
				mappedLevel, err := MapLogLevel(value)
				if err != nil && logLevel != unknown {
					t.Error(err)
				}

				if mappedLevel != logLevel {
					t.Errorf("expected log level to be %s, not: %s\n", logLevel, mappedLevel)
				}
			})
		}
	}
}

func TestLogger_SetLevelFromString(t *testing.T) {
	traces := []string{"trace", "TRACE"}
	debugs := []string{"debug", "DEBUG"}
	warns := []string{"warn", "WARN"}
	infos := []string{"info", "INFO"}
	errors := []string{"error", "ERROR"}
	criticals := []string{"critical", "CRITICAL"}
	offs := []string{"off", "OFF"}
	unknowns := []string{"foo"}

	var unknown LogLevel = -1
	m := map[LogLevel][]string{
		TRACE:    traces,
		DEBUG:    debugs,
		WARN:     warns,
		INFO:     infos,
		ERROR:    errors,
		CRITICAL: criticals,
		OFF:      offs,
		unknown:  unknowns,
	}

	for logLevel, values := range m {
		for _, value := range values {
			t.Run(value, func(t *testing.T) {
				logger := &Logger{}
				err := logger.SetLevelFromString(value)
				if err != nil {
					if logLevel != unknown {
						t.Error(err)
					}
				} else if logger.LogLevel != logLevel {
					t.Errorf("expected log level to be %s, not: %s\n", logLevel, logger.LogLevel)
				}
			})
		}
	}
}
