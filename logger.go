package go_logger

import (
	"encoding/json"
	"fmt"
	log "github.com/cihub/seelog"
)

const (
	OFF               LogLevel = iota
	TRACE             LogLevel = iota
	DEBUG             LogLevel = iota
	INFO              LogLevel = iota
	WARN              LogLevel = iota
	ERROR             LogLevel = iota
	CRITICAL          LogLevel = iota
	DEFAULT_LOG_LEVEL          = INFO
)

type UnknownLogLevel string

func (e UnknownLogLevel) Error() string {
	return fmt.Sprintf("Unknown log level: %s", string(e))
}

type LogData map[string]interface{}

// Wrapper around any third-party logger api.
// This way I can swap log libraries relatively easily in this single location.
type Logger struct {
	seeLog   log.LoggerInterface
	LogLevel LogLevel
}

type LogLevel int

func (l LogLevel) String() string {
	switch l {
	case TRACE:
		return "TRACE"
	case DEBUG:
		return "DEBUG"
	case INFO:
		return "INFO"
	case WARN:
		return "WARN"
	case ERROR:
		return "ERROR"
	case CRITICAL:
		return "CRITICAL"
	case OFF:
		return "OFF"
	default:
		return "UNKNOWN"
	}
}

func LoggerFromConfigFile(file string) (*Logger, error) {
	logger, err := log.LoggerFromConfigAsFile(file)
	if err != nil {
		return nil, err
	}

	logger.SetAdditionalStackDepth(1) //don't log file line # here, but where log was invoked before here
	return &Logger{seeLog: logger, LogLevel: DEFAULT_LOG_LEVEL}, err
}

func (l *Logger) Debug(data *LogData) {
	if l.canLog(DEBUG) {
		formatted, _ := formatData(data)
		l.seeLog.Debug(formatted)
	}
}

func (l *Logger) Debugf(format string, v *LogData) {
	if l.canLog(DEBUG) {
		l.seeLog.Debugf(format, v)
	}
}

func (l *Logger) Warn(data *LogData) {
	if l.canLog(WARN) {
		formatted, _ := formatData(data)
		l.seeLog.Warn(formatted)
	}
}

func (l *Logger) Warnf(format string, v *LogData) {
	if l.canLog(WARN) {
		l.seeLog.Warnf(format, v)
	}
}

func (l *Logger) Info(data *LogData) {
	if l.canLog(INFO) {
		formatted, _ := formatData(data)
		l.seeLog.Info(formatted)
	}
}

func (l *Logger) Infof(format string, v *LogData) {
	if l.canLog(INFO) {
		l.seeLog.Infof(format, v)
	}
}

func (l *Logger) Error(data *LogData) {
	if l.canLog(ERROR) {
		formatted, _ := formatData(data)
		l.seeLog.Error(formatted)
	}
}

func (l *Logger) Errorf(format string, v *LogData) {
	if l.canLog(ERROR) {
		l.seeLog.Errorf(format, v)
	}
}

func (l *Logger) Critical(data *LogData) {
	if l.canLog(CRITICAL) {
		formatted, _ := formatData(data)
		l.seeLog.Critical(formatted)
	}
}

func (l *Logger) Criticalf(format string, v *LogData) {
	if l.canLog(CRITICAL) {
		l.seeLog.Criticalf(format, v)
	}
}

func (l *Logger) Flush() {
	l.seeLog.Flush()
}

func (l *Logger) canLog(level LogLevel) bool {
	return l.LogLevel != OFF && l.LogLevel <= level
}

func MapLogLevel(level string) (LogLevel, error) {
	switch level {
	case "trace", "TRACE":
		return TRACE, nil
	case "debug", "DEBUG":
		return DEBUG, nil
	case "warn", "WARN":
		return WARN, nil
	case "info", "INFO":
		return INFO, nil
	case "error", "ERROR":
		return ERROR, nil
	case "critical", "CRITICAL":
		return CRITICAL, nil
	case "":
		return DEFAULT_LOG_LEVEL, nil
	case "off", "OFF":
		return OFF, nil
	default:
		return -1, UnknownLogLevel(level)
	}
}

func (l *Logger) SetLevelFromString(level string) error {
	mappedLevel, err := MapLogLevel(level)
	if err != nil {
		return err
	}
	l.LogLevel = mappedLevel

	return nil
}

// formats LogData into JSON format
func formatData(data *LogData) (string, error) {
	prefix := ""
	indent := " "
	b, err := json.MarshalIndent(data, prefix, indent)
	// TODO: How to handle potential Marshalling errors?
	return string(b), err
}

// TODO: Do I want other LogData formats?
